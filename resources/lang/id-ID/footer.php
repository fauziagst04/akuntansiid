<?php

return [

    'version'               => 'Versi',
    'powered'               => 'Didukung oleh Akutansi.id',
    'link'                  => 'https://akaunting.com',
    'software'              => 'Perangkat Lunak Akutansi',

];
